## Android Sample App

This app showcases a number of Android frameworks and best practices including:

AndroidX and JetPack

Activities

Fragments

Coroutines

Lifecycle

FragmentFactory

ViewModel Factory

MVVM

LiveData

Flow

Material Design

Dependency Injection

Navigation Component

View Binding

Data Binding

Room Database

Caching

Retrofit

RecyclerView

ConstraintLayout

SwipeRefreshLayout

CardView

Toolbar

Colors

Styles and Themes

Unit testing

UI testing

Mockito

Espresso

FragmentScenario
