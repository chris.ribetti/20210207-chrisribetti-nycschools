package com.application

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.application.ui.ErrorFragment
import com.application.utils.*
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ErrorFragmentTests {
    private val navController = getTestNavController(R.id.ErrorFragment)

    @Test
    fun error_fragment_tests() {
        launchTestFragment(
            ErrorFragment::class.java,
            navController
        )
        onView(R.id.errorFragment).verifyIsDisplayed()
        onView(R.id.message).verifyIsDisplayed()
        onView(R.id.try_again).verifyIsDisplayed()
        onView(R.id.back_to_schools).verifyIsNotDisplayed()
    }
}