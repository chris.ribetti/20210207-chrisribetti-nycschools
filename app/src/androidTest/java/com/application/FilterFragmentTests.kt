package com.application

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.application.ui.ErrorFragment
import com.application.ui.FilterFragment
import com.application.utils.*
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class FilterFragmentTests {
    private val navController = getTestNavController(R.id.FilterFragment)

    @Test
    fun filter_fragment_tests() {
        launchTestFragment(
            FilterFragment::class.java,
            navController
        )
        onView(R.id.message)
            .verifyIsDisplayed()
            .verifyTextEquals(R.string.set_filtering)
        onView(R.id.schoolTextInputLayout).verifyIsDisplayed()
        onView(R.id.schoolEditText)
            .verifyIsDisplayed()
            .verifyTextEquals(R.string.school, hint = true)
        onView(R.id.and)
            .verifyIsDisplayed()
            .verifyTextEquals(R.string.and)
        onView(R.id.cityOrZipTextInputLayout).verifyIsDisplayed()
        onView(R.id.cityOrZipEditText)
            .verifyIsDisplayed()
            .verifyTextEquals(R.string.cityOrZip, hint = true)
        onView(R.id.button)
            .verifyIsDisplayed()
            .verifyTextEquals(R.string.apply)
    }
}