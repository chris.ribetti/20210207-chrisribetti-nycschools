package com.application

import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.application.ui.SchoolItemAdapter
import com.application.ui.SchoolsFragment
import com.application.utils.getTestNavController
import com.application.utils.launchTestFragment
import com.application.utils.onView
import com.application.utils.verifyIsDisplayed
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SchoolsFragmentTests {
    private val navController = getTestNavController(R.id.SchoolsFragment)

    @Test
    fun schools_fragment_tests() {
        launchTestFragment(
            SchoolsFragment::class.java,
            navController
        )
        onView(R.id.schoolsFragment).verifyIsDisplayed()
        onView(R.id.recyclerView).verifyIsDisplayed()
            .perform(
                RecyclerViewActions
                    .actionOnItemAtPosition<SchoolItemAdapter.ViewHolder>(0, click())
            )
        assertThat(navController.currentDestination?.id).isEqualTo(R.id.ScoresFragment)
    }
}


