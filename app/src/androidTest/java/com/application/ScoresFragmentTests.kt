package com.application

import androidx.core.os.bundleOf
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.application.ui.ScoresFragment
import com.application.utils.*
import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ScoresFragmentTests {
    private val navController = getTestNavController(R.id.ScoresFragment)

    @Test
    fun scores_fragment_tests_show_scores() {
        launchTestFragment(
            ScoresFragment::class.java,
            navController,
            bundleOf(ScoresFragment.DBN to "dbn 1")
        )
        onView(R.id.scoresFragment).verifyIsDisplayed()
        onView(R.id.name).verifyIsDisplayed()
        onView(R.id.numTakers).verifyIsDisplayed()
        onView(R.id.mathScore).verifyIsDisplayed()
        onView(R.id.writingScore).verifyIsDisplayed()
        onView(R.id.readingScore).verifyIsDisplayed()
    }

    @Test
    fun scores_fragment_tests_show_not_available() {
        launchTestFragment(
            ScoresFragment::class.java,
            navController,
            bundleOf(ScoresFragment.DBN to "")
        )
        onView(R.id.scoresFragment).verifyIsDisplayed()
        onView(R.id.name)
            .verifyIsDisplayed()
            .verifyTextEquals(R.string.scores_not_available)
        onView(R.id.numTakers).verifyIsNotDisplayed()
        onView(R.id.mathScore).verifyIsNotDisplayed()
        onView(R.id.writingScore).verifyIsNotDisplayed()
        onView(R.id.readingScore).verifyIsNotDisplayed()
    }
}


