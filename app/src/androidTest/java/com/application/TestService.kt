package com.application

import androidx.test.platform.app.InstrumentationRegistry
import com.application.api.MainService
import com.application.data.MainRepository
import com.application.data.SchoolItem
import com.application.data.ScoreItem
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class TestService : MainService {
    private val schoolsList = jsonToSchoolItemList(fileToJson("schools.json"))
    private val scoresMap = jsonToScoreItemList(fileToJson("scores.json"))
        .map { it.dbn to it }.toMap()

    override suspend fun getSchools(map: Map<String, String>): List<SchoolItem> {
        return schoolsList
    }

    override suspend fun getScores(map: Map<String, String>): List<ScoreItem> {
        return listOf(scoresMap[map[MainRepository.DBN_QUERY_KEY]] ?: ScoreItem())
    }

    private fun fileToJson(fileName: String): String {
        if (fileName.isEmpty()) return ""
        return try {
            val inputStream =
                InstrumentationRegistry.getInstrumentation().context.assets.open(fileName)
            inputStream.bufferedReader().readText()
        } catch (e: Exception) {
            return ""
        }
    }

    private fun jsonToSchoolItemList(json: String): List<SchoolItem> {
        val type: Type = object : TypeToken<List<SchoolItem?>?>() {}.type
        return try {
            Gson().fromJson(json, type)
        } catch (e: Exception) {
            emptyList()
        }
    }

    private fun jsonToScoreItemList(json: String): List<ScoreItem> {
        val type: Type = object : TypeToken<List<ScoreItem?>?>() {}.type
        return try {
            Gson().fromJson(json, type)
        } catch (e: Exception) {
            emptyList()
        }
    }
}