package com.application.utils

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.testing.FragmentScenario
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavHostController
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.test.core.app.ApplicationProvider
import com.application.R
import com.application.ui.ErrorFragment
import com.application.ui.FilterFragment
import com.application.ui.SchoolsFragment
import com.application.ui.ScoresFragment

fun getTestNavController(@IdRes res: Int): NavHostController {
    return TestNavHostController(ApplicationProvider.getApplicationContext()).apply {
        Handler(Looper.getMainLooper()).post {
            setGraph(R.navigation.main_nav_graph)
            setCurrentDestination(res)
        }
    }
}

fun <F : Fragment> launchTestFragment(
    cls: Class<F>,
    navController: NavHostController? = null,
    args: Bundle? = null
) {
    val factory =
        MainFragmentFactory(
            TestInjectionUtils.getTestViewModelFactory()
        )

    val scenario =
        FragmentScenario.launchInContainer(
            cls,
            args,
            R.style.Theme_Application,
            factory
        )

    if (navController != null) {
        scenario.onFragment { fragment ->
            Navigation.setViewNavController(
                fragment.requireView(),
                navController
            )
        }
    }
}

private class MainFragmentFactory(private val factory: ViewModelProvider.Factory) :
    FragmentFactory() {
    override fun instantiate(classLoader: ClassLoader, className: String): Fragment {
        return when (loadFragmentClass(
            classLoader,
            className
        )) {
            ErrorFragment::class.java -> ErrorFragment()
            FilterFragment::class.java -> FilterFragment { factory }
            SchoolsFragment::class.java -> SchoolsFragment { factory }
            ScoresFragment::class.java -> ScoresFragment { factory }
            else -> super.instantiate(classLoader, className)
        }
    }
}