package com.application.utils

import androidx.lifecycle.ViewModelProvider
import com.application.InjectionUtils
import com.application.TestService
import com.application.api.MainService
import com.application.data.MainRepository
import com.application.viewmodels.MainViewModelFactory

class TestInjectionUtils {
    companion object {
        private fun getTestService(): MainService {
            return TestService()
        }

        private fun getTestRepository(): MainRepository {
            return MainRepository(
                getTestService(),
                InjectionUtils.getCacheDao()
            )
        }

        fun getTestViewModelFactory(): ViewModelProvider.Factory {
            return MainViewModelFactory(getTestRepository())
        }
    }
}
