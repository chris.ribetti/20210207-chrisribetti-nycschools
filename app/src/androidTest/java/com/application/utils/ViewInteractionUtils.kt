package com.application.utils

import androidx.annotation.IdRes
import androidx.test.espresso.Espresso
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import org.hamcrest.CoreMatchers

fun onView(@IdRes id: Int): ViewInteraction =
    Espresso.onView(withId(id))

fun ViewInteraction.verifyTextEquals(
    @IdRes id: Int = -1,
    text: String = "",
    hint: Boolean = false,
    contentDescription: Boolean = false
): ViewInteraction =
    if (hint) {
        if (id == -1) check(ViewAssertions.matches(ViewMatchers.withHint(text))) else check(
            ViewAssertions.matches(ViewMatchers.withHint(id))
        )
    } else if (contentDescription) {
        if (id == -1) check(ViewAssertions.matches(ViewMatchers.withContentDescription(text))) else check(
            ViewAssertions.matches(ViewMatchers.withContentDescription(id))
        )
    } else {
        if (id == -1) check(ViewAssertions.matches(ViewMatchers.withText(text))) else check(
            ViewAssertions.matches(ViewMatchers.withText(id))
        )
    }

fun ViewInteraction.verifyIsDisplayed(): ViewInteraction =
    check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

fun ViewInteraction.verifyIsNotDisplayed(): ViewInteraction =
    check(ViewAssertions.matches(CoreMatchers.not(ViewMatchers.isDisplayed())))

fun ViewInteraction.doClick(): ViewInteraction =
    perform(ViewActions.click())