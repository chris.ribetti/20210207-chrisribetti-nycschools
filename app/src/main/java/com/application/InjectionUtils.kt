package com.application

import androidx.lifecycle.ViewModelProvider
import com.application.api.MainService
import com.application.data.MainCache
import com.application.data.MainDao
import com.application.data.MainDatabase
import com.application.data.MainRepository
import com.application.viewmodels.MainViewModelFactory

class InjectionUtils {
    companion object {
        private fun getService(): MainService {
            return MainService.create()
        }

        private fun getRoomDao(): MainDao {
            return MainDatabase.getInstance(MainApplication.applicationContext()).mainDao()
        }

        fun getCacheDao(): MainDao {
            return MainCache()
        }

        private fun getRepository(): MainRepository {
            return MainRepository(
                getService(),
                getRoomDao() // getRoomDao or getCacheDao
            )
        }

        fun getViewModelFactory(): ViewModelProvider.Factory {
            return MainViewModelFactory(getRepository())
        }
    }
}
