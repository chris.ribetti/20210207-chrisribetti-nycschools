package com.application.api

sealed class ApiResponse<out T : Any> {
    object ApiLoading : ApiResponse<Nothing>()
    class ApiSuccessResponse<out T : Any>(val data: T) : ApiResponse<T>()
    class ApiErrorResponse(val msg: String) : ApiResponse<Nothing>()
}