package com.application.api

import com.application.data.SchoolItem
import com.application.data.ScoreItem
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.QueryMap

interface MainService {

    @Headers("X-App-Token: TlnUZghDzdZu5SfUCn9dLU8ry")
    @GET("s3k6-pzi2.json")
    suspend fun getSchools(@QueryMap map: Map<String, String>): List<SchoolItem>

    @Headers("X-App-Token: TlnUZghDzdZu5SfUCn9dLU8ry")
    @GET("f9bf-2cp4.json")
    suspend fun getScores(@QueryMap map: Map<String, String>): List<ScoreItem>

    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us/resource/"

        fun create(): MainService {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(
                    OkHttpClient
                        .Builder()
                        .addInterceptor(
                            HttpLoggingInterceptor()
                                .setLevel(HttpLoggingInterceptor.Level.BODY)
                        )
                        .build()
                )
                .build()
                .create(MainService::class.java)
        }
    }
}