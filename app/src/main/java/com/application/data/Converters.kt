package com.application.data

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type

class Converters {
    private val gson = Gson()

    @TypeConverter
    fun fromList(list: List<SchoolItem>): String {
        val type: Type = object : TypeToken<List<SchoolItem>>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toList(json: String): List<SchoolItem>? {
        val type: Type = object : TypeToken<List<SchoolItem?>?>() {}.type
        return gson.fromJson<List<SchoolItem>>(json, type)
    }
}