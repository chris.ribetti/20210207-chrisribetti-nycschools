package com.application.data

data class FilterItem(
    val school: String = "",
    val cityOrZip: String = "",
    val shouldFilter: Boolean = false
)