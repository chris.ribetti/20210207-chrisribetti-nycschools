package com.application.data

import androidx.collection.LruCache
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

class MainCache : MainDao {
    private var schools = Schools()
    private val scores = LruCache<String, ScoreItem>(MAX_SCORE_ITEMS)

    // @Insert(onConflict = REPLACE)
    override suspend fun saveSchools(schools: Schools) {
        this.schools = schools
    }

    // @Query("SELECT * FROM schools")
    override fun loadSchools(): Flow<Schools> {
        return flowOf(schools)
    }

    //@Query("SELECT EXISTS(SELECT 1 FROM schools WHERE lastModified > :currentTime - :timeout LIMIT 1)")
    override suspend fun isSchoolsDataFreshInternal(timeout: Long, currentTime: Long): Boolean {
        return (schools.schools.isNotEmpty() && schools.lastModified > currentTime - timeout)
    }

    // @Insert(onConflict = REPLACE)
    override suspend fun saveScores(score: ScoreItem) {
        scores.put(score.dbn, score)
    }

    // @Query("SELECT * FROM scoreitem WHERE dbn = :dbn")
    override fun loadScores(dbn: String): Flow<ScoreItem> {
        return flowOf(scores.get(dbn) ?: ScoreItem())
    }

    // @Query("SELECT EXISTS(SELECT 1 FROM scoreitem WHERE dbn = :dbn AND lastModified > :currentTime - :timeout LIMIT 1)")
    override suspend fun isScoresDataFreshInternal(
        dbn: String,
        timeout: Long,
        currentTime: Long
    ): Boolean {
        return scores[dbn]?.run { this.lastModified > currentTime - timeout } ?: false
    }

    companion object {
        private const val MAX_SCORE_ITEMS = 2
    }
}