package com.application.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface MainDao {
    @Insert(onConflict = REPLACE)
    suspend fun saveSchools(schools: Schools)

    @Query("SELECT * FROM schools")
    fun loadSchools(): Flow<Schools>

    @Query("SELECT EXISTS(SELECT 1 FROM schools WHERE lastModified > :currentTime - :timeout LIMIT 1)")
    suspend fun isSchoolsDataFreshInternal(
        timeout: Long,
        currentTime: Long = System.currentTimeMillis()
    ): Boolean

    suspend fun isSchoolsDataFresh(timeout: Long) =
        isSchoolsDataFreshInternal(timeout)

    @Insert(onConflict = REPLACE)
    suspend fun saveScores(score: ScoreItem)

    @Query("SELECT * FROM scoreitem WHERE dbn = :dbn")
    fun loadScores(dbn: String): Flow<ScoreItem>

    @Query("SELECT EXISTS(SELECT 1 FROM scoreitem WHERE dbn = :dbn AND lastModified > :currentTime - :timeout LIMIT 1)")
    suspend fun isScoresDataFreshInternal(
        dbn: String,
        timeout: Long,
        currentTime: Long = System.currentTimeMillis()
    ): Boolean

    suspend fun isScoresDataFresh(dbn: String, timeout: Long) =
        isScoresDataFreshInternal(dbn, timeout)
}
