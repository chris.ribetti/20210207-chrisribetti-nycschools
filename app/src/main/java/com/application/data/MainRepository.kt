package com.application.data

import com.application.api.ApiResponse
import com.application.api.MainService
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import java.util.concurrent.TimeUnit

open class MainRepository(
    private val service: MainService,
    private val dao: MainDao
) {
    open fun getSchools(forceRefresh: Boolean) = flow {
        emit(ApiResponse.ApiLoading)
        if (!refreshSchools(forceRefresh)) {
            emit(ApiResponse.ApiErrorResponse(SCHOOLS_ERROR))
        } else {
            dao.loadSchools()
                .catch {
                    emit(ApiResponse.ApiErrorResponse(SCHOOLS_ERROR))
                }
                .collect {
                    emit(ApiResponse.ApiSuccessResponse(it.schools))
                }
        }
    }

    private suspend fun refreshSchools(forceRefresh: Boolean): Boolean {
        if (!forceRefresh && dao.isSchoolsDataFresh(FRESH_TIMEOUT))
            return true
        return try {
            dao.saveSchools(
                Schools(
                    service.getSchools(getSchoolsQueryMap)
                )
            )
            true
        } catch (e: Exception) {
            false
        }
    }

    open fun getScores(dbn: String) = flow {
        emit(ApiResponse.ApiLoading)
        if (!refreshScores(dbn)) {
            emit(ApiResponse.ApiErrorResponse(SCORES_ERROR))
        } else {
            dao.loadScores(dbn)
                .catch {
                    emit(ApiResponse.ApiErrorResponse(SCORES_ERROR))
                }
                .collect {
                    emit(ApiResponse.ApiSuccessResponse(it))
                }
        }
    }

    private suspend fun refreshScores(dbn: String): Boolean {
        if (dao.isScoresDataFresh(dbn, FRESH_TIMEOUT))
            return true
        return try {
            dao.saveScores(
                service.getScores(mapOf(DBN_QUERY_KEY to dbn))[0]
            )
            true
        } catch (e: Exception) {
            false
        }
    }

    companion object {
        val getSchoolsQueryMap = mapOf("\$order" to "school_name")
        val FRESH_TIMEOUT = TimeUnit.DAYS.toMillis(30)
        const val SCHOOLS_ERROR = "error in getSchools"
        const val SCORES_ERROR = "error in getScores"
        const val DBN_QUERY_KEY = "dbn"
    }
}