package com.application.data

data class SchoolItem(
    val dbn: String = "",
    val school_name: String = "",
    val location: String = "",
    val primary_address_line_1: String = "",
    val city: String = "",
    val zip: String = "",
    val neighborhood: String = "",
    val phone_number: String = "",
    val school_email: String = "",
    val website: String = ""
)