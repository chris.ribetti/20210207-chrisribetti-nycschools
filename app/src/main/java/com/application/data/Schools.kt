package com.application.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Schools(
    val schools: List<SchoolItem> = emptyList(),
    val lastModified: Long = System.currentTimeMillis(),
    @PrimaryKey val tag: Int = 0
)