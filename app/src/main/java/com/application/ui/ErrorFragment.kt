package com.application.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.application.R
import com.application.databinding.FragmentErrorBinding

class ErrorFragment : Fragment(R.layout.fragment_error) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentErrorBinding.bind(view)

        val msgType = arguments?.getInt(MSG_TYPE) ?: SCHOOLS_FAILED
        binding.message.text =
            if (msgType == SCHOOLS_FAILED)
                getString(R.string.error_getting_schools)
            else {
                binding.backToSchools.visibility = View.VISIBLE
                getString(R.string.error_getting_scores)
            }

        binding.tryAgain.apply {
            setOnClickListener {
                findNavController().navigateUp()
            }
        }

        binding.backToSchools.apply {
            setOnClickListener {
                findNavController().navigateUp().apply {
                    findNavController().navigateUp()
                }
            }
        }
    }

    companion object {
        const val MSG_TYPE = "MSG_TYPE"
        const val SCHOOLS_FAILED = 0
        const val SCORES_FAILED = 1
    }
}