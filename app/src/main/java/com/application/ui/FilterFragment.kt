package com.application.ui

import android.content.Context.INPUT_METHOD_SERVICE
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.application.R
import com.application.data.FilterItem
import com.application.databinding.FragmentFilterBinding
import com.application.viewmodels.MainViewModel

class FilterFragment @JvmOverloads constructor(factory: (() -> ViewModelProvider.Factory)? = null) :
    Fragment(R.layout.fragment_filter) {
    private val viewModel: MainViewModel by activityViewModels(factory)
    private var shouldFilter = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentFilterBinding.bind(view)

        binding.button.setOnClickListener {
            shouldFilter = true
            val item = FilterItem(
                school = binding.schoolEditText.text.toString(),
                cityOrZip = binding.cityOrZipEditText.text.toString(),
                shouldFilter = true
            )
            viewModel.filterCriteria.postValue(item)
            findNavController().navigateUp()
        }

        viewModel.filterCriteria.observe(viewLifecycleOwner, Observer<FilterItem> { result ->
            binding.schoolEditText.apply {
                setText(result.school)
                setSelection(result.school.length)
                showKeyboard(this)
            }
            binding.cityOrZipEditText.setText(result.cityOrZip)
        })
    }

    override fun onDestroy() {
        if (!shouldFilter) {
            viewModel.filterCriteria.postValue(viewModel.filterCriteria.value?.copy(shouldFilter = false))
        }
        hideKeyboard()
        super.onDestroy()
    }

    private fun showKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = activity?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    private fun hideKeyboard() {
        activity?.currentFocus?.let {
            val imm = activity?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }
}