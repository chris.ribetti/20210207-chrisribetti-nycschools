package com.application.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.RecyclerView
import com.application.R
import com.application.data.SchoolItem
import com.application.databinding.LayoutItemBinding

class SchoolItemAdapter(
    private val fragment: Fragment,
    private val items: List<SchoolItem>
) : RecyclerView.Adapter<SchoolItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        val newItem = item.copy(
            school_name = item.school_name.stripExtendedChars(),
            school_email = item.school_email.run {
                if (this.isEmpty()) fragment.getString(R.string.email_address_not_available) else this
            }
        )
        holder.bind(newItem)
        holder.itemView.setOnClickListener {
            NavHostFragment.findNavController(fragment).navigate(
                R.id.action_to_ScoresFragment,
                bundleOf(ScoresFragment.DBN to item.dbn)
            )
        }
    }

    private fun String.stripExtendedChars(): String {
        val regex = Regex("[^A-Za-z0-9 _-]")
        return regex.replace(this, "")
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder constructor(private val binding: LayoutItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(schoolItem: SchoolItem) {
            binding.schoolItem = schoolItem
            binding.executePendingBindings()
        }
    }
}