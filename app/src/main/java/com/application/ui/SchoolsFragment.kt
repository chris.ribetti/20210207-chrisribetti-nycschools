package com.application.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.application.R
import com.application.api.ApiResponse
import com.application.databinding.FragmentSchoolsBinding
import com.application.viewmodels.MainViewModel

class SchoolsFragment @JvmOverloads constructor(factory: (() -> ViewModelProvider.Factory)? = null) :
    Fragment(R.layout.fragment_schools) {
    private val viewModel: MainViewModel by activityViewModels(factory)
    private var firstTime = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentSchoolsBinding.bind(view)
        val progressBar = binding.progressBar.progressBar

        val recyclerView = binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = SchoolItemAdapter(this@SchoolsFragment, viewModel.items)
        }

        binding.swipeRefresh.setOnRefreshListener {
            binding.swipeRefresh.isRefreshing = false
            viewModel.getSchools(forceRefresh = true)
        }

        viewModel.schoolData.observe(viewLifecycleOwner, { result ->
            when (result) {
                is ApiResponse.ApiLoading -> {
                    progressBar.visibility = View.VISIBLE
                }
                is ApiResponse.ApiSuccessResponse -> {
                    firstTime = false
                    recyclerView.scrollToPosition(0)
                    recyclerView.adapter?.notifyDataSetChanged()
                    progressBar.visibility = View.GONE
                    recyclerView.visibility = View.VISIBLE
                }
                is ApiResponse.ApiErrorResponse -> {
                    findNavController().navigate(
                        R.id.action_to_ErrorFragment,
                        bundleOf(ErrorFragment.MSG_TYPE to ErrorFragment.SCHOOLS_FAILED)
                    )
                }
            }
        })

        viewModel.filterCriteria.observe(viewLifecycleOwner, {
            if (it.shouldFilter) {
                viewModel.filter()
                recyclerView.scrollToPosition(0)
                recyclerView.adapter?.notifyDataSetChanged()
            }
        })

        if (firstTime) {
            firstTime = false
            viewModel.getSchools(forceRefresh = false)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_filter -> {
                findNavController().navigate(R.id.action_to_FilterFragment)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}