package com.application.ui

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.application.R
import com.application.api.ApiResponse
import com.application.databinding.FragmentScoresBinding
import com.application.viewmodels.MainViewModel

class ScoresFragment @JvmOverloads constructor(factory: (() -> ViewModelProvider.Factory)? = null) :
    Fragment(R.layout.fragment_scores) {
    private val viewModel: MainViewModel by activityViewModels(factory)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val dbn = arguments?.getString(DBN) ?: ""

        val binding = FragmentScoresBinding.bind(view)
        val progressBar = binding.progressBar.progressBar
        val group = binding.group

        binding.button.apply {
            setOnClickListener {
                findNavController().navigateUp()
            }
        }

        viewModel.scoreData.observe(viewLifecycleOwner, { result ->
            when (result) {
                is ApiResponse.ApiLoading -> {
                    progressBar.visibility = View.VISIBLE
                }
                is ApiResponse.ApiSuccessResponse -> {
                    if (result.data.dbn.isEmpty()) {
                        binding.name.text = getString(R.string.scores_not_available)
                        progressBar.visibility = View.GONE
                        binding.name.visibility = View.VISIBLE
                        binding.button.visibility = View.VISIBLE
                    } else {
                        val item = result.data
                        binding.name.text = item.school_name
                        binding.numTakers.text =
                            getString(
                                R.string.number_of_takers,
                                item.num_of_sat_test_takers.toIntStringOrNA()
                            )
                        binding.mathScore.text =
                            getString(
                                R.string.math_score_average,
                                item.sat_math_avg_score.toIntStringOrNA()
                            )
                        binding.writingScore.text =
                            getString(
                                R.string.writing_score_average,
                                item.sat_writing_avg_score.toIntStringOrNA()
                            )
                        binding.readingScore.text = getString(
                            R.string.reading_score_average,
                            item.sat_critical_reading_avg_score.toIntStringOrNA()
                        )
                        progressBar.visibility = View.GONE
                        group.visibility = View.VISIBLE
                    }
                }
                is ApiResponse.ApiErrorResponse -> {
                    findNavController().navigate(
                        R.id.action_to_ErrorFragment,
                        bundleOf(ErrorFragment.MSG_TYPE to ErrorFragment.SCORES_FAILED)
                    )
                }
            }
        })

        group.visibility = View.GONE
        progressBar.visibility = View.GONE
        viewModel.getScores(dbn)
    }

    private fun String.toIntStringOrNA(): String {
        return this.toIntOrNull()?.let { this } ?: "n/a"
    }

    companion object {
        const val DBN = "DBN"
    }
}