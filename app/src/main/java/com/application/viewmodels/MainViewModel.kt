package com.application.viewmodels

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.application.api.ApiResponse
import com.application.data.FilterItem
import com.application.data.MainRepository
import com.application.data.SchoolItem
import com.application.data.ScoreItem
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

typealias SchoolResponse = ApiResponse<List<SchoolItem>>
typealias ScoreResponse = ApiResponse<ScoreItem>

class MainViewModel(private val repository: MainRepository) : ViewModel() {

    private val _schoolData: MutableLiveDataUpdates<SchoolResponse> = MutableLiveDataUpdates()
    val schoolData: LiveData<SchoolResponse> = _schoolData

    private val _scoreData: MutableLiveDataUpdates<ScoreResponse> = MutableLiveDataUpdates()
    val scoreData: LiveData<ScoreResponse> = _scoreData

    val items = mutableListOf<SchoolItem>()
    val filterCriteria: MutableLiveData<FilterItem> = MutableLiveData(FilterItem())

    @VisibleForTesting
    val unfilteredItems = mutableListOf<SchoolItem>()
    
    fun getSchools(forceRefresh: Boolean) {
        unfilteredItems.clear()
        viewModelScope.launch {
            repository.getSchools(forceRefresh).collect { result ->
                when (result) {
                    is ApiResponse.ApiLoading,
                    is ApiResponse.ApiErrorResponse -> {
                    }
                    is ApiResponse.ApiSuccessResponse -> {
                        items.clear()
                        items.addAll(result.data)
                        unfilteredItems.addAll(result.data)
                        if (filterCriteria.value?.shouldFilter == true) filter()
                    }
                }
                _schoolData.postValue(result)
            }
        }
    }

    fun filter() {
        val value = filterCriteria.value ?: return
        if (!value.shouldFilter) return
        items.clear()
        val list = unfilteredItems.filter {
            it.school_name.myContains(value.school) &&
                    (it.city.myContains(value.cityOrZip) || it.zip.myContains(value.cityOrZip))
        }.toList()
        items.addAll(list)
        return
    }

    private fun String.myContains(s2: String): Boolean {
        if (s2.isEmpty()) return true
        return this.contains(s2, ignoreCase = true)
    }

    fun getScores(dbn: String) {
        viewModelScope.launch {
            repository.getScores(dbn).collect { response ->
                _scoreData.postValue(response)
            }
        }
    }
}