package com.application.viewmodels

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import java.util.concurrent.atomic.AtomicBoolean

/**
 * LiveData that calls onChanged to new observer only after data changes. If there is data available
 * when the observer becomes active, the observer will not receive the data. See: SingleLiveEvent
 */
class MutableLiveDataUpdates<T> : MutableLiveData<T> {
    constructor() : super()
    constructor(value: T) : super(value)

    private val pending = mutableListOf<AtomicBoolean>()
    private val observers = mutableListOf<Observer<in T>>()

    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<in T>) {
        pending.add(AtomicBoolean(false))
        observers.add(observer)

        val index = observers.size - 1
        super.observe(owner, { t: T ->
            if (pending[index].compareAndSet(true, false)) {
                observers[index].onChanged(t)
            }
        })
    }

    @MainThread
    override fun setValue(t: T) {
        for (i in pending.indices) pending[i].set(true)
        super.setValue(t)
    }

    override fun postValue(t: T) {
        for (i in pending.indices) pending[i].set(true)
        super.postValue(t)
    }
}