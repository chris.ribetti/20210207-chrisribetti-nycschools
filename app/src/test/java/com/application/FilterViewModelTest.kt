package com.application

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.application.data.FilterItem
import com.application.data.MainRepository
import com.application.data.SchoolItem
import com.application.viewmodels.MainViewModel
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock

@ExperimentalCoroutinesApi
class FilterViewModelTest {
    private lateinit var repository: MainRepository
    private lateinit var viewModel: MainViewModel

    private val items = listOf(
        SchoolItem(school_name = "high school 1", city = "brooklyn", zip = "10001"),
        SchoolItem(school_name = "high school 2", city = "queens", zip = "10002"),
        SchoolItem(school_name = "academy 1", city = "brooklyn", zip = "10003"),
        SchoolItem(school_name = "academy 2", city = "queens", zip = "10004")
    )

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(TestCoroutineDispatcher())

        repository = mock(MainRepository::class.java)
        viewModel = MainViewModel(repository)
        viewModel.items.addAll(items)
        viewModel.unfilteredItems.addAll(items)

        assertThat(viewModel.items.size).isEqualTo(4)
        assertThat(viewModel.unfilteredItems.size).isEqualTo(4)
    }

    @After
    fun teardown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `filter view model should not filter no criteria`() = runBlocking {
        // given...
        viewModel.filterCriteria.value = FilterItem(shouldFilter = false)
        // when...
        viewModel.filter()
        // then...
        assertThat(viewModel.items.size).isEqualTo(4)
    }

    @Test
    fun `filter view model should not filter with criteria`() = runBlocking {
        // given...
        viewModel.filterCriteria.value = FilterItem(school = "high school", shouldFilter = false)
        // when...
        viewModel.filter()
        // then...
        assertThat(viewModel.items.size).isEqualTo(4)
    }

    @Test
    fun `filter view model no filter criteria`() = runBlocking {
        // given...
        // when...
        viewModel.filter()
        // then...
        assertThat(viewModel.items.size).isEqualTo(4)
    }

    @Test
    fun `filter view model filter school`() = runBlocking {
        // given...
        viewModel.filterCriteria.value = FilterItem(school = "high school", shouldFilter = true)
        // when...
        viewModel.filter()
        // then...
        assertThat(viewModel.items.size).isEqualTo(2)
    }

    @Test
    fun `filter view model filter city`() = runBlocking {
        // given...
        viewModel.filterCriteria.value = FilterItem(cityOrZip = "brooklyn", shouldFilter = true)
        // when...
        viewModel.filter()
        // then...
        assertThat(viewModel.items.size).isEqualTo(2)
    }

    @Test
    fun `filter view model filter zip code`() = runBlocking {
        // given...
        viewModel.filterCriteria.value = FilterItem(cityOrZip = "10001", shouldFilter = true)
        // when...
        viewModel.filter()
        // then...
        assertThat(viewModel.items.size).isEqualTo(1)
    }

    @Test
    fun `filter view model filter school and zip code match`() = runBlocking {
        // given...
        viewModel.filterCriteria.value =
            FilterItem(school = "high school", cityOrZip = "10001", shouldFilter = true)
        // when...
        viewModel.filter()
        // then...
        assertThat(viewModel.items.size).isEqualTo(1)
    }

    @Test
    fun `filter view model filter school and zip code no match`() = runBlocking {
        // given...
        viewModel.filterCriteria.value =
            FilterItem(school = "academy", cityOrZip = "10001", shouldFilter = true)
        // when...
        viewModel.filter()
        // then...
        assertThat(viewModel.items.size).isEqualTo(0)
    }

    @Test
    fun `filter view model filter school and city match`() = runBlocking {
        // given...
        viewModel.filterCriteria.value =
            FilterItem(school = "high school", cityOrZip = "brooklyn", shouldFilter = true)
        // when...
        viewModel.filter()
        // then...
        assertThat(viewModel.items.size).isEqualTo(1)
    }

    @Test
    fun `filter view model filter school and city no match`() = runBlocking {
        // given...
        viewModel.filterCriteria.value =
            FilterItem(school = "high school", cityOrZip = "bronx", shouldFilter = true)
        // when...
        viewModel.filter()
        // then...
        assertThat(viewModel.items.size).isEqualTo(0)
    }
}
