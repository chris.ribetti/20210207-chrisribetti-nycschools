package com.application

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.application.api.ApiResponse
import com.application.api.MainService
import com.application.data.MainDao
import com.application.data.MainRepository
import com.application.data.SchoolItem
import com.application.data.Schools
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock

@ExperimentalCoroutinesApi
class GetSchoolsRepositoryTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val timeout = MainRepository.FRESH_TIMEOUT

    @Test
    fun `get schools repository success get cache`() = runBlocking {
        // given...
        val expectedResponse = listOf(
            SchoolItem(dbn = "1"),
            SchoolItem(dbn = "2")
        )
        val service = mock(MainService::class.java)
        val dao = mock(MainDao::class.java)
        val repository = MainRepository(service, dao)

        whenever(dao.isSchoolsDataFresh(eq(timeout))).thenReturn(true)
        whenever(dao.loadSchools()).thenReturn(flowOf(Schools(expectedResponse)))

        // when...
        val result = repository.getSchools(forceRefresh = false).toList()

        // then...
        assertThat(result.size).isEqualTo(2)
        assertThat(result[0]).isInstanceOf(ApiResponse.ApiLoading::class.java)
        assertThat(result[1]).isInstanceOf(ApiResponse.ApiSuccessResponse::class.java)
        assertThat((result[1] as ApiResponse.ApiSuccessResponse).data).isEqualTo(expectedResponse)
    }

    @Test
    fun `get schools repository success get api`() = runBlocking {
        // given...
        val expectedResponse = listOf(
            SchoolItem(dbn = "1"),
            SchoolItem(dbn = "2")
        )
        val service = mock(MainService::class.java)
        val dao = mock(MainDao::class.java)
        val repository = MainRepository(service, dao)

        whenever(dao.isSchoolsDataFresh(eq(timeout))).thenReturn(false)
        whenever(dao.loadSchools()).thenReturn(flowOf(Schools(expectedResponse)))
        whenever(service.getSchools(MainRepository.getSchoolsQueryMap)).thenReturn(expectedResponse)

        // when...
        val result = repository.getSchools(forceRefresh = false).toList()

        // then...
        assertThat(result.size).isEqualTo(2)
        assertThat(result[0]).isInstanceOf(ApiResponse.ApiLoading::class.java)
        assertThat(result[1]).isInstanceOf(ApiResponse.ApiSuccessResponse::class.java)
        assertThat((result[1] as ApiResponse.ApiSuccessResponse).data).isEqualTo(expectedResponse)
    }

    @Test
    fun `get schools repository dao save exception`() = runBlocking {
        // given...
        val service = mock(MainService::class.java)
        val dao = mock(MainDao::class.java)
        val repository = MainRepository(service, dao)

        whenever(dao.isSchoolsDataFresh(eq(timeout))).thenReturn(false)
        whenever(dao.saveSchools(any())).thenThrow(RuntimeException())

        // when...
        val result = repository.getSchools(forceRefresh = false).toList()

        // then...
        assertThat(result.size).isEqualTo(2)
        assertThat(result[0]).isInstanceOf(ApiResponse.ApiLoading::class.java)
        assertThat(result[1]).isInstanceOf(ApiResponse.ApiErrorResponse::class.java)
        assertThat((result[1] as ApiResponse.ApiErrorResponse).msg).isEqualTo(MainRepository.SCHOOLS_ERROR)
    }

    @Test
    fun `get schools repository dao load exception`() = runBlocking {
        // given...
        val service = mock(MainService::class.java)
        val dao = mock(MainDao::class.java)
        val repository = MainRepository(service, dao)

        whenever(dao.isSchoolsDataFresh(eq(timeout))).thenReturn(true)
        whenever(dao.loadSchools()).thenReturn(flow { error("any message") })

        // when...
        val result = repository.getSchools(forceRefresh = false).toList()

        // then...
        assertThat(result.size).isEqualTo(2)
        assertThat(result[0]).isInstanceOf(ApiResponse.ApiLoading::class.java)
        assertThat(result[1]).isInstanceOf(ApiResponse.ApiErrorResponse::class.java)
        assertThat((result[1] as ApiResponse.ApiErrorResponse).msg).isEqualTo(MainRepository.SCHOOLS_ERROR)
    }

    @Test
    fun `get schools repository api exception`() = runBlocking {
        // given...
        val service = mock(MainService::class.java)
        val dao = mock(MainDao::class.java)
        val repository = MainRepository(service, dao)

        whenever(dao.isSchoolsDataFresh(eq(timeout))).thenReturn(false)
        whenever(service.getSchools(MainRepository.getSchoolsQueryMap)).thenThrow(RuntimeException())

        // when...
        val result = repository.getSchools(forceRefresh = false).toList()

        // then...
        assertThat(result.size).isEqualTo(2)
        assertThat(result[0]).isInstanceOf(ApiResponse.ApiLoading::class.java)
        assertThat(result[1]).isInstanceOf(ApiResponse.ApiErrorResponse::class.java)
        assertThat((result[1] as ApiResponse.ApiErrorResponse).msg).isEqualTo(MainRepository.SCHOOLS_ERROR)
    }
}