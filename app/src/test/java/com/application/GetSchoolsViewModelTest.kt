package com.application

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.application.api.ApiResponse
import com.application.data.MainRepository
import com.application.data.SchoolItem
import com.application.viewmodels.MainViewModel
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*

@ExperimentalCoroutinesApi
class GetSchoolsViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(TestCoroutineDispatcher())
    }

    @After
    fun teardown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `get schools view model success`() = runBlocking {
        // given...
        val expectedSchoolData = listOf(
            SchoolItem(dbn = "1"),
            SchoolItem(dbn = "2")
        )
        val flow = flow {
            emit(ApiResponse.ApiLoading)
            emit(ApiResponse.ApiSuccessResponse(expectedSchoolData))
        }
        val repository = mock(MainRepository::class.java)

        whenever(repository.getSchools(ArgumentMatchers.anyBoolean())).thenReturn(flow)

        val viewModel = MainViewModel(repository)
        val observer: Observer<ApiResponse<List<SchoolItem>>> = mock()
        viewModel.schoolData.observeForever(observer)

        // when...
        viewModel.getSchools(forceRefresh = false)

        // then...
        argumentCaptor<ApiResponse<List<SchoolItem>>>().run {
            verify(observer, times(2)).onChanged(capture())
            val (loading, success) = allValues
            assertThat(loading).isInstanceOf(ApiResponse.ApiLoading::class.java)
            assertThat(success).isInstanceOf(ApiResponse.ApiSuccessResponse::class.java)
            assertThat((success as ApiResponse.ApiSuccessResponse).data).isEqualTo(
                expectedSchoolData
            )
        }
    }

    @Test
    fun `get schools view model exception`() = runBlocking {
        // given...
        val flow = flow {
            emit(ApiResponse.ApiLoading)
            emit(ApiResponse.ApiErrorResponse(MainRepository.SCHOOLS_ERROR))
        }
        val repository = mock(MainRepository::class.java)

        whenever(repository.getSchools(ArgumentMatchers.anyBoolean())).thenReturn(flow)

        val viewModel = MainViewModel(repository)
        val observer: Observer<ApiResponse<List<SchoolItem>>> = mock()
        viewModel.schoolData.observeForever(observer)

        // when...
        viewModel.getSchools(forceRefresh = false)

        // then...
        argumentCaptor<ApiResponse<List<SchoolItem>>>().run {
            verify(observer, times(2)).onChanged(capture())
            val (loading, error) = allValues
            assertThat(loading).isInstanceOf(ApiResponse.ApiLoading::class.java)
            assertThat(error).isInstanceOf(ApiResponse.ApiErrorResponse::class.java)
            assertThat((error as ApiResponse.ApiErrorResponse).msg).isEqualTo(MainRepository.SCHOOLS_ERROR)
        }
    }
}
