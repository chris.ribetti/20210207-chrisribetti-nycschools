package com.application

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.application.api.ApiResponse
import com.application.api.MainService
import com.application.data.MainDao
import com.application.data.MainRepository
import com.application.data.ScoreItem
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock

@ExperimentalCoroutinesApi
class GetScoresRepositoryTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val timeout = MainRepository.FRESH_TIMEOUT

    @Test
    fun `get scores repository success get cache`() = runBlocking {
        // given...
        val dbn = "1"
        val expectedResponse = ScoreItem(dbn)
        val service = mock(MainService::class.java)
        val dao = mock(MainDao::class.java)
        val repository = MainRepository(service, dao)

        whenever(dao.isScoresDataFresh(eq(dbn), eq(timeout))).thenReturn(true)
        whenever(dao.loadScores(dbn)).thenReturn(flowOf(expectedResponse))

        // when...
        val result = repository.getScores(dbn).toList()

        // then...
        assertThat(result.size).isEqualTo(2)
        assertThat(result[0]).isInstanceOf(ApiResponse.ApiLoading::class.java)
        assertThat(result[1]).isInstanceOf(ApiResponse.ApiSuccessResponse::class.java)
        assertThat((result[1] as ApiResponse.ApiSuccessResponse).data).isEqualTo(expectedResponse)
    }

    @Test
    fun `get scores repository success get api`() = runBlocking {
        // given...
        val dbn = "1"
        val expectedResponse = ScoreItem(dbn)
        val service = mock(MainService::class.java)
        val dao = mock(MainDao::class.java)
        val repository = MainRepository(service, dao)

        whenever(dao.isScoresDataFresh(eq(dbn), eq(timeout))).thenReturn(false)
        whenever(dao.loadScores(dbn)).thenReturn(flowOf(expectedResponse))
        whenever(service.getScores(mapOf(MainRepository.DBN_QUERY_KEY to dbn))).thenReturn(
            listOf(
                expectedResponse
            )
        )

        // when...
        val result = repository.getScores(dbn).toList()

        // then...
        assertThat(result.size).isEqualTo(2)
        assertThat(result[0]).isInstanceOf(ApiResponse.ApiLoading::class.java)
        assertThat(result[1]).isInstanceOf(ApiResponse.ApiSuccessResponse::class.java)
        assertThat((result[1] as ApiResponse.ApiSuccessResponse).data).isEqualTo(expectedResponse)
    }

    @Test
    fun `get scores repository dao save exception`() = runBlocking {
        // given...
        val dbn = "1"
        val service = mock(MainService::class.java)
        val dao = mock(MainDao::class.java)
        val repository = MainRepository(service, dao)

        whenever(dao.isScoresDataFresh(eq(dbn), eq(timeout))).thenReturn(false)
        whenever(dao.saveScores(any())).thenThrow(RuntimeException())

        // when...
        val result = repository.getScores(dbn).toList()

        // then...
        assertThat(result.size).isEqualTo(2)
        assertThat(result[0]).isInstanceOf(ApiResponse.ApiLoading::class.java)
        assertThat(result[1]).isInstanceOf(ApiResponse.ApiErrorResponse::class.java)
        assertThat((result[1] as ApiResponse.ApiErrorResponse).msg).isEqualTo(MainRepository.SCORES_ERROR)
    }

    @Test
    fun `get scores repository dao load exception`() = runBlocking {
        // given...
        val dbn = "1"
        val service = mock(MainService::class.java)
        val dao = mock(MainDao::class.java)
        val repository = MainRepository(service, dao)

        whenever(dao.isScoresDataFresh(eq(dbn), eq(timeout))).thenReturn(true)
        whenever(dao.loadScores(dbn)).thenReturn(flow { error("any message") })

        // when...
        val result = repository.getScores(dbn).toList()

        // then...
        assertThat(result.size).isEqualTo(2)
        assertThat(result[0]).isInstanceOf(ApiResponse.ApiLoading::class.java)
        assertThat(result[1]).isInstanceOf(ApiResponse.ApiErrorResponse::class.java)
        assertThat((result[1] as ApiResponse.ApiErrorResponse).msg).isEqualTo(MainRepository.SCORES_ERROR)
    }

    @Test
    fun `get scores repository api exception`() = runBlocking {
        // given...
        val dbn = "1"
        val service = mock(MainService::class.java)
        val dao = mock(MainDao::class.java)
        val repository = MainRepository(service, dao)

        whenever(dao.isScoresDataFresh(eq(dbn), eq(timeout))).thenReturn(false)
        whenever(service.getScores(mapOf(MainRepository.DBN_QUERY_KEY to dbn))).thenThrow(
            RuntimeException()
        )

        // when...
        val result = repository.getScores(dbn).toList()

        // then...
        assertThat(result.size).isEqualTo(2)
        assertThat(result[0]).isInstanceOf(ApiResponse.ApiLoading::class.java)
        assertThat(result[1]).isInstanceOf(ApiResponse.ApiErrorResponse::class.java)
        assertThat((result[1] as ApiResponse.ApiErrorResponse).msg).isEqualTo(MainRepository.SCORES_ERROR)
    }
}