package com.application

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.application.api.ApiResponse
import com.application.data.MainRepository
import com.application.viewmodels.MainViewModel
import com.application.data.ScoreItem
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.*

@ExperimentalCoroutinesApi
class GetScoresViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        Dispatchers.setMain(TestCoroutineDispatcher())
    }

    @After
    fun teardown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `get scores view model success`() = runBlocking {
        // given...
        val dbn = "1"
        val expectedScoreData = ScoreItem(dbn)
        val flow = flow {
            emit(ApiResponse.ApiLoading)
            emit(ApiResponse.ApiSuccessResponse(expectedScoreData))
        }
        val repository = mock(MainRepository::class.java)

        whenever(repository.getScores(dbn)).thenReturn(flow)

        val viewModel = MainViewModel(repository)
        val observer: Observer<ApiResponse<ScoreItem>> = mock()
        viewModel.scoreData.observeForever(observer)

        // when...
        viewModel.getScores(dbn)

        // then...
        argumentCaptor<ApiResponse<ScoreItem>>().run {
            verify(observer, times(2)).onChanged(capture())
            val (loading, success) = allValues
            assertThat(loading).isInstanceOf(ApiResponse.ApiLoading::class.java)
            assertThat(success).isInstanceOf(ApiResponse.ApiSuccessResponse::class.java)
            assertThat((success as ApiResponse.ApiSuccessResponse).data).isEqualTo(expectedScoreData)
        }
    }

    @Test
    fun `get scores view model exception`() = runBlocking {
        // given...
        val dbn = "1"
        val flow = flow {
            emit(ApiResponse.ApiLoading)
            emit(ApiResponse.ApiErrorResponse(MainRepository.SCORES_ERROR))
        }
        val repository = mock(MainRepository::class.java)

        whenever(repository.getScores(dbn)).thenReturn(flow)

        val viewModel = MainViewModel(repository)
        val observer: Observer<ApiResponse<ScoreItem>> = mock()
        viewModel.scoreData.observeForever(observer)

        // when...
        viewModel.getScores(dbn)

        // then...
        argumentCaptor<ApiResponse<ScoreItem>>().run {
            verify(observer, times(2)).onChanged(capture())
            val (loading, error) = allValues
            assertThat(loading).isInstanceOf(ApiResponse.ApiLoading::class.java)
            assertThat(error).isInstanceOf(ApiResponse.ApiErrorResponse::class.java)
            assertThat((error as ApiResponse.ApiErrorResponse).msg).isEqualTo(MainRepository.SCORES_ERROR)
        }
    }
}
