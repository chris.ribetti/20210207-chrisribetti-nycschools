package com.application

import org.mockito.ArgumentCaptor
import org.mockito.Mockito
import org.mockito.stubbing.OngoingStubbing


inline fun <reified T> mock(): T = Mockito.mock(T::class.java)
inline fun <reified T> argumentCaptor(): ArgumentCaptor<T> = ArgumentCaptor.forClass(T::class.java)
inline fun <reified T> any(): T = Mockito.any(T::class.java)
fun <T> whenever(methodCall: T): OngoingStubbing<T> = Mockito.`when`(methodCall)
fun <T> eq(value: T): T = Mockito.eq(value) ?: value

